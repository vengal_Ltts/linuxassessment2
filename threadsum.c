#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>

#define MAX 20 //length os array
#define Thread_MAX 4 //you mention as many threads if required

int a[MAX]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
int sum_th[Thread_MAX]={0};
int thread_no=0;

void* sum(void* arg)
{
	int i,num = thread_no++;
	for(i=num*(MAX / Thread_MAX);i<(num+1)*(MAX / Thread_MAX);i++)
	{
		sum_th[num] +=a[i];
	}
	
}

int main()
{
	int i;
	pthread_t threads[Thread_MAX];
	for(i=0;i<Thread_MAX;i++)
		pthread_create(&threads[i],NULL,sum,(void*)NULL);
	for(i=0;i<Thread_MAX;i++)
		pthread_join(threads[i],NULL);
	int totalsum=0;
	for(i=0;i<Thread_MAX;i++)
	{
		totalsum +=sum_th[i];
	}
	printf("total sum is %d\n",totalsum);
	return 0;
}

