#include<stdio.h>
#include<sys/resource.h>
#include<sys/time.h>

int main()
{
	struct rlimit s;
	int ch;
	printf("----------------------------------------------------------\n");
	printf("RESOURCE\t\t Current Limit\t MaxLimit\n");
	printf("----------------------------------------------------------\n");
	getrlimit(RLIMIT_CPU,&s);
	printf("CPU time\t");
	printf("\t\t%ld\t",s.rlim_cur);
	printf("\t%ld",s.rlim_max);
	

	getrlimit(RLIMIT_FSIZE,&s);
	printf("\nFile size");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_DATA,&s);
	printf("\nProcss Data sgment");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);
	getrlimit(RLIMIT_STACK,&s);
	printf("\nProcess Stack");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_CORE,&s);
	printf("\nCore Files Size");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_RSS,&s);
	printf("\nBytes for process");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_NPROC,&s);
	printf("\nNumber of processes");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);


	getrlimit(RLIMIT_NOFILE,&s);
	printf("\nFile descriptor ");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_MEMLOCK,&s);
	printf("\nLocked Memory ");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t%ld",s.rlim_max);


	getrlimit(RLIMIT_AS,&s);
	printf("\nVirtual Memory ");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_LOCKS,&s);
	printf("\nlocking fcntl calls ");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_SIGPENDING,&s);
	printf("\nQueued Signals ");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_MSGQUEUE,&s);
	printf("\nPosix msg Queues ");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_NICE,&s);
	printf("\nNice value");
	printf("\t\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_RTPRIO,&s);
	printf("\nReal time priority ");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld",s.rlim_max);

	getrlimit(RLIMIT_RTTIME,&s);
	printf("\nReal time scheduling");
	printf("\t\t%ld",s.rlim_cur);
	printf("\t\t%ld\n",s.rlim_max);

	return 0;
}
