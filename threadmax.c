#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>

#define MAX 20 //length os array
#define Thread_MAX 4 //you mention as many threads if required

int a[MAX]={23,45,12,34,67,87,89,90,9,98,88,76,54,32,21,13,25,36,47,58};
int max_th[Thread_MAX]={0};
int thread_no=0;

void* maximum(void* arg)
{
	int i,num = thread_no++;
	int maxs=0;
	for(i=num*(MAX / Thread_MAX);i<(num+1)*(MAX / Thread_MAX);i++)
	{
		if(a[i]>maxs)
			maxs=a[i];
	}
	max_th[num]=maxs;
	
}

int main()
{
	int i,maxs=0;
	pthread_t threads[Thread_MAX];
	for(i=0;i<Thread_MAX;i++)
		pthread_create(&threads[i],NULL,maximum,(void*)NULL);
	for(i=0;i<Thread_MAX;i++)
		pthread_join(threads[i],NULL);
	int totalsum=0;
	for(i=0;i<Thread_MAX;i++)
	{
		if(max_th[i]>maxs)
			maxs=max_th[i];
	}
	printf("maximum value is %d\n",maxs);
	return 0;
}

