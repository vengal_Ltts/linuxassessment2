#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int main(int argc,char* argv[])
{
	int ret,i,status;
	printf("welcome..pid=%d\n",getpid());
	ret=fork();
	if(ret<0)
	{
		perror("fork");
		exit(1);
	}
	if(ret==0)
	{
		printf("child--welcome,pid=%d,ppid=%d\n",
			getpid(),getppid());
		int k;
		k=execlp(argv[1],argv[1],argv[2],argv[3],NULL);
		if(k<0)
		{
			perror("execv");
			exit(1);
		}
		exit(0);
	}
	else	//ret>0
	{
		printf("parent--hello,pid=%d,ppid=%d\n",
			getpid(),getppid());
		waitpid(-1,&status,0); //wait(&status);
		printf("parent--child exit status=%d\n",
			WEXITSTATUS(status));
	}
	return 0;
}

