#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
int main(int argc, char* argv[])
{

	int  fp1, fp2;
	char ch;
	int n;
	if(argc<3)
	{
		printf("Please provide valid number of arguments(2 files)\n");;
		return 1;
	}
	
	fp1 = open(argv[1],O_RDONLY);
	if(fp1<0)
	{
		perror("error opening fle");
		exit(1);
	}
	fp2= open(argv[2],O_WRONLY|O_CREAT,0777);
	if(fp2<0)
	{
		perror("error opening 2nd file");
		exit(1);
	}
	do
	{
		n=read(fp1,&ch,1);
		write(fp2,&ch,n);			
		if(n==-1)
		{
			perror("error reading character\n");
			exit(1);
		}		

	}while(n!=0);
	
	close(fp1);
	close(fp2);

	printf("successfully copied\n");
	return 0;
}
